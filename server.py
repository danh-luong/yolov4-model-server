from flask import Flask, request
from flask_restful import Api, Resource
import base64
from yolov3.utils import detect_image, detect_realtime, detect_video, Load_Yolo_model, detect_video_realtime_mp
import numpy as np
from yolov3.configs import *
import cv2

app = Flask(__name__)
api = Api(app)

if __name__ == '__main__':
    app.run(debug=True)

yolo = Load_Yolo_model(type = 'yolov4')

class UploadImage(Resource):
    def post(self):
        json_data = request.json
        file_upload = json_data['image']
        listPosition = {}
        if file_upload:
            image_decoded_bytes = base64.b64decode(file_upload)
            npimg = np.fromstring(image_decoded_bytes, dtype=np.uint8)
            original_image = cv2.imdecode(npimg, 1)
            original_image = cv2.cvtColor(original_image, cv2.COLOR_BGR2RGB)
            original_image = cv2.cvtColor(original_image, cv2.COLOR_BGR2RGB)
            listPosition = detect_image(yolo, original_image, input_size=YOLO_INPUT_SIZE, CLASSES = YOLO_COCO_CLASSES, rectangle_colors=(255,0,0), helmet_flag=True)
        return listPosition

api.add_resource(UploadImage, "/detect_image")