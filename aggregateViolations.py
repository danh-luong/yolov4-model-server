import collections
import math
import sys
from utils.geometric_util import *
from test_package.print_frame import print_out_violation_picture, print_frame_process
import copy


def dist(p1, p2):
    return math.sqrt((p1.x - p2.x) * (p1.x - p2.x) * 2 + (p1.y - p2.y) * (p1.y - p2.y))


def dist_bottom(p1, p2):
    p1_x = (p1.left + p1.right) / 2
    p2_x = (p2.left + p2.right) / 2
    p1_y = p1.bottom
    p2_y = p2.bottom

    return math.sqrt((p1_x - p2_x) * (p1_x - p2_x) * 2.5 + (p1_y - p2_y) * (p1_y - p2_y) / 2)


def findClosest(moto, list):
    min_dis = sys.maxsize
    for iterator in list:
        min_dis = min(dist_bottom(moto, iterator), min_dis)
    return min_dis


def updateFrame(F1, F2):
    result = []

    if len(F2) == 0:
        return F1
    if len(F1) == 0:
        return F2

    # print("len lan luot :", len(F1), " - ", len(F2))
    ratio_bound = 1.15
    for loop1 in F1:
        current = updateMotorbike(loop1, loop1)
        temp = copy.copy(current)
        for next in F2:
            is_change = False
            cls_to_F1 = findClosest(current, F2)
            cls_to_F2 = findClosest(next, F1)
            if (cls_to_F1 == cls_to_F2) and (dist_bottom(current, next) == cls_to_F1):
                is_change = True
                if dist_bottom(current, next) > (min(calculateDistance(current), calculateDistance(next)) * ratio_bound):
                    is_change = False
                if (current.bottom < next.bottom) and (dist_bottom(current, next)>(current.right-current.left)/2):
                    is_change = False
            if is_change:
                temp = updateMotorbike(next, next)
                if temp.vio_pas_rate >= current.vio_pas_rate:
                    current.vio_pas_rate = temp.vio_pas_rate
                    current.img_pas = temp.img_pas

                    # update picture location
                    current.pas_l = temp.left
                    current.pas_r = temp.right
                    current.pas_t = temp.top
                    current.pas_b = temp.bottom

                    # print("update vio_pas_rate = ", current.vio_pas_rate)
                if temp.vio_cro_rate >= current.vio_cro_rate:
                    current.vio_cro_rate = temp.vio_cro_rate
                    current.img_cro = temp.img_cro

                    # update picture location
                    current.cro_l = temp.left
                    current.cro_r = temp.right
                    current.cro_t = temp.top
                    current.cro_b = temp.bottom

                if temp.vio_enc_rate >= current.vio_enc_rate:
                    current.vio_enc_rate = temp.vio_enc_rate
                    current.img_enc = temp.img_enc

                    # update picture location
                    current.enc_l = temp.left
                    current.enc_r = temp.right
                    current.enc_t = temp.top
                    current.enc_b = temp.bottom

                # tong hop lai diem vi pham
                current.vio_hel_rate = current.vio_hel_rate + temp.vio_hel_rate
                current.count_hel += 1
                if temp.vio_hel_rate == 100.0:
                    current.img_hel = temp.img_hel

                    # update picture location
                    current.hel_l = temp.left
                    current.hel_r = temp.right
                    current.hel_t = temp.top
                    current.hel_b = temp.bottom

                if temp.vio_wro_rate >= current.vio_wro_rate:
                    current.vio_wro_rate = temp.vio_wro_rate
                    current.img_wro = temp.img_wro

                    # update picture location
                    current.wro_l = temp.left
                    current.wro_r = temp.right
                    current.wro_t = temp.top
                    current.wro_b = temp.bottom

                current.top = copy.copy(temp.top)
                current.bottom = copy.copy(temp.bottom)
                current.left = copy.copy(temp.left)
                current.right = copy.copy(temp.right)
                current.x = copy.copy(temp.x)
                current.y = copy.copy(temp.y)
        result.append(current)

    for loop1 in F2:
        current = copy.copy(loop1)
        current = updateMotorbike(current, current)
        is_match = False
        for next in F1:
            cls_to_F1 = findClosest(current, F1)
            cls_to_F2 = findClosest(next, F2)
            if (cls_to_F1 == cls_to_F2) and (dist_bottom(current, next) == cls_to_F1):
                is_match = True
                if dist_bottom(current, next) > calculateDistance(current) * ratio_bound:
                    is_match = False
                if (current.bottom > next.bottom) and (dist_bottom(current, next) > (current.right-current.left)/2):
                    is_match = False
        # print("is_match = ", is_match)
        if not is_match:
            result.append(current)

    # print("total result before = ", len(result))
    count = -1
    final_res = []

    while count < len(result) - 1:
        count += 1
        mark = -1
        is_dup = False
        for iterator in range(0, len(final_res)-1):
            check_motor = final_res[iterator]
            if (result[count].x == check_motor.x) and (result[count].y == check_motor.y):
                if result[count].count_hel > check_motor.count_hel:
                    mark = iterator
                is_dup = True
        if not is_dup:
            final_res.append(result[count])
        if mark != -1:
            final_res[mark] = result[count]
    # print("total result after = ", len(final_res))

    return final_res


def process(LF):
    LF = collections.OrderedDict(sorted(LF.items()))
    print("len = ", len(LF))

    if len(LF) == 1:
        return LF[1]
    else:
        current_frame = LF[1]
        for frame in range(1, (len(LF)) + 1):
            print("Frame number ", frame)
            current_frame = updateFrame(current_frame, LF[frame])
            print_frame_process(current_frame)

        for moto in current_frame:
            moto.vio_hel_rate = moto.vio_hel_rate / (moto.count_hel + 1)

        return current_frame


def updateMotorbike(motorbike, update):
    temp = copy.copy(motorbike)
    # print("toa do ne : ", update.left, " - ", update.right, " - ", update.top, " - ", update.bottom)

    temp.img_cro = update.img_cro
    temp.img_enc = update.img_enc
    temp.img_pas = update.img_pas
    temp.img_hel = update.img_hel
    temp.img_wro = update.img_wro

    temp.pas_l = update.left
    temp.cro_l = update.left
    temp.enc_l = update.left
    temp.hel_l = update.left
    temp.wro_l = update.left

    temp.pas_r = update.right
    temp.cro_r = update.right
    temp.enc_r = update.right
    temp.hel_r = update.right
    temp.wro_r = update.right

    temp.pas_t = update.top
    temp.cro_t = update.top
    temp.enc_t = update.top
    temp.hel_t = update.top
    temp.wro_t = update.top

    temp.pas_b = update.bottom
    temp.cro_b = update.bottom
    temp.enc_b = update.bottom
    temp.hel_b = update.bottom
    temp.wro_b = update.bottom

    return temp
