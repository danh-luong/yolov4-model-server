import copy

from utils.geometric_util import calculateOverlapRate


def related_people_to_motorbike(motorbike, people):
    list_result = []
    for person in people:
        if person.bottom < (motorbike.bottom + (motorbike.bottom - motorbike.top) / 10):
            resized_person = copy.copy(person)
            resized_person.top = (person.top + person.bottom) / 2
            overlapRate = calculateOverlapRate(motorbike, resized_person)
            if 0.25 <= overlapRate <= 1:
                list_result.append(person)

    return list_result


def related_helmet_to_people(target_person, helmets):
    # print("HELMET - PEOPLE")
    result = 0
    max_overlap = 0
    person = copy.copy(target_person)
    person.bottom = (person.top + person.bottom) / 2
    for helmet in helmets:
        if (helmet.bottom < person.top) or (helmet.right < person.left) or (helmet.left > person.right):
            result = 0
        else:
            current_overlap = calculateOverlapRate(helmet, person)
            if (current_overlap > max_overlap) and (current_overlap > 0.3):
                max_overlap = current_overlap
                result = 1
    if result == 0:
        return False
    return True


def print_location(object):
    print(object.left, " - ", object.top, " - ", object.right, " - ", object.bottom)
