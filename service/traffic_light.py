def is_red(traffic_lights):
     result = False
     for trafficLight in traffic_lights:
          if (trafficLight.red):
               result = True
               break
     return result
