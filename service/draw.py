import cv2

# image, left, top, right, bottom
def draw(image, x1, y1, x2, y2):
    cv2.rectangle(image, (x1, y1), (x2, y2), (50, 50, 240), 2)
    return image

def draw_line(image, x1, y1, x2, y2):
    cv2.line(image, (x1, y1), (x2, y2), (50, 50, 240), 2)
    return image
