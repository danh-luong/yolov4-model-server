from location.LinePoint import LinePoint
from case_resolve import *
from database_access import *
from location.LinePoint import LinePoint
from location.ObjectLocation import printObject
from location.Point import Point
from service.helmet_service import related_people_to_motorbike
from service.lane_service import calculate_distance
from service.license_plate_service import check_license_plate
from service.license_plate_service import related_motorbike_to_license
from service.motorbike_service import detect_crossing
from service.motorbike_service import detect_encroaching
from service.motorbike_service import detect_not_wearing_helmet
from service.motorbike_service import detect_passing_red_light


def detect_right_violation(list_position=[], horizontalLine=LinePoint(0, 0, 0, 0), upperBound=LinePoint(0, 0, 0, 0),
                           verticalLine=LinePoint(0, 0, 0, 0), frame=0, leftBound=LinePoint(0, 0, 0, 0),
                           rightBound=LinePoint(0, 0, 0, 0)):
    # print("detecting processing")
    lane_distance = calculate_distance(horizontalLine, upperBound)
    motorbike_locations = list_position['MotorbikeLocation']
    person_locations = list_position['PersonLocation']
    helmet_locations = list_position['HelmetLocation']
    result = []
    for motorbike in motorbike_locations:
        # printObject(motorbike)
        if check_valid_right_motorbike(list_position, motorbike, frame, horizontalLine, upperBound, leftBound,
                                       rightBound):
            motorbike.vio_pas_rate = detect_passing_red_light(motorbike, horizontalLine, lane_distance)
            motorbike.vio_cro_rate = detect_crossing(motorbike, horizontalLine)
            motorbike.vio_enc_rate = detect_encroaching(motorbike, verticalLine)
            motorbike.vio_hel_rate = detect_not_wearing_helmet(motorbike, person_locations, helmet_locations)

            if motorbike.vio_pas_rate < 0:
                motorbike.vio_pas_rate = 0
            # if motorbike.vio_pas_rate > 0:
            #     motorbike.vio_cro_rate = 0

            motorbike.img_pas = frame
            motorbike.img_cro = frame
            motorbike.img_enc = frame
            motorbike.img_hel = frame

            # printObject(motorbike)
            print("new bike added")
            result.append(motorbike)

    # for record in result:
    #     print(record)

    return result


def detect_right_green_violation(list_position=[], horizontalLine=LinePoint(0, 0, 0, 0), lane=LinePoint(0, 0, 0, 0),
                                 verticalLine=LinePoint(0, 0, 0, 0), frame=0, leftBound=LinePoint(0, 0, 0, 0),
                                 rightBound=LinePoint(0, 0, 0, 0)):
    motorbike_locations = list_position['MotorbikeLocation']
    person_locations = list_position['PersonLocation']
    helmet_locations = list_position['HelmetLocation']
    result = []
    for motorbike in motorbike_locations:
        # printObject(motorbike)
        if check_valid_right_motorbike(list_position, motorbike, frame, horizontalLine, lane, leftBound, rightBound):
            motorbike.vio_enc_rate = detect_encroaching(motorbike, verticalLine)
            motorbike.vio_hel_rate = detect_not_wearing_helmet(motorbike, person_locations, helmet_locations)
            motorbike.img_hel = frame
            motorbike.img_enc = frame

            print("NEW MOTORBIKE")
            # printObject(motorbike)
            print("new bike added")
            result.append(motorbike)
    # for record in result:
    #     print(record)

    return result


def detect_left_violation(list_position=[], horizontalLine=LinePoint(0, 0, 0, 0), upperBound=LinePoint(0, 0, 0, 0),
                          verticalLine=LinePoint(0, 0, 0, 0), frame=0, leftBound=LinePoint(0, 0, 0, 0),
                          rightBound=LinePoint(0, 0, 0, 0), camera_id=0):
    location = load_line(camera_id)
    motorbike_locations = list_position['MotorbikeLocation']
    license_location = list_position['LicenseLocation']
    person_locations = list_position['PersonLocation']
    helmet_locations = list_position['HelmetLocation']

    result = []

    upperLimit = Point((leftBound.left + leftBound.right) / 2, (leftBound.top + leftBound.bottom) / 2)

    for license in license_location:
        if (leftBound.left < license.x < rightBound.left) and (upperBound.top > license.y > horizontalLine.bottom):
            # insert wrong lane
            crop_license_img = frame[license.top: license.bottom, license.left:license.right]
            license_number = get_license_number(crop_license_img)
            file_name = '5' + str(get_time()) + '.jpg'

            motorbike = related_motorbike_to_license(license, motorbike_locations)
            if motorbike != '':
                storage_picture = draw_motorbike(
                    motorbike.frame, motorbike.left, motorbike.top, motorbike.right, motorbike.bottom)
                storage_picture = put_Text(storage_picture, motorbike.left, motorbike.top, 'wrong lane')
                # storage_picture = draw_bound(storage_picture, list_bounding)
                cv2.imwrite(file_name, storage_picture)
                image_url = save('5' + file_name, file_name)
                image_id = insert_image(image_url, camera_id)
                print("co loi wrong lane ne - ", image_id)
                insert_case(5, location, license_number, image_id)

                rate = detect_not_wearing_helmet(motorbike, person_locations, helmet_locations)
                if rate == 100:
                    # insert not wearing helmet
                    crop_license_img = frame[license.top: license.bottom, license.left:license.right]
                    license_number = get_license_number(crop_license_img)
                    file_name = '4' + str(get_time()) + '.jpg'
                    storage_picture = draw_motorbike(
                        motorbike.frame, motorbike.left, motorbike.top, motorbike.right, motorbike.bottom)
                    storage_picture = put_Text(storage_picture, motorbike.left, motorbike.top, 'not wearing helmet')
                    # storage_picture = draw_bound(storage_picture, list_bounding)
                    cv2.imwrite(file_name, storage_picture)
                    image_url = save('4' + file_name, file_name)
                    image_id = insert_image(image_url, camera_id)
                    print("co loi helmet ne - ", image_id)
                    insert_case(4, location, license_number, image_id)
    return True


def check_valid_right_motorbike(list_position=[], motorbike='', frame=0, horizontalLine=LinePoint(0, 0, 0, 0),
                                upperBound=LinePoint(0, 0, 0, 0), leftBound=LinePoint(0, 0, 0, 0),
                                rightBound=LinePoint(0, 0, 0, 0)):

    if motorbike.bottom < ((upperBound.top + upperBound.bottom) / 2):
        # print("Reject error: Vuot can tren")
        # print("bound = ", (upperBound.top + upperBound.bottom) / 2)
        return False
    # if motorbike.top < limit_top:
    #     print("Reject error: Vuot can tren")
    #     return False

    if motorbike.right < min(leftBound.left, leftBound.right):
        # print("Reject error: Xe vuot can trai")
        # print("xe phai = ", motorbike.right, "can trai = ", min(leftBound.left, leftBound.right))
        return False

    if motorbike.top < (leftBound.top + leftBound.bottom)/2:
        if motorbike.right < leftBound.left:
            # print("Reject error: Xe vuot can trai")
            return False
    else:
        if motorbike.right < (leftBound.left + leftBound.right)/2:
            return False

    if motorbike.left > max(rightBound.left, rightBound.right):
        # print("Reject error: Xe vuot can phai")
        # print("can phai = ", max(rightBound.left, rightBound.right))
        return False


    if motorbike.top < (rightBound.top + rightBound.bottom)/2:
        if motorbike.left > rightBound.left:
            # print("Reject error: Xe vuot can phai")
            return False
    else:
        if motorbike.left > (rightBound.left + rightBound.right)/2:
            return False

    person_locations = list_position['PersonLocation']
    list_related_person = related_people_to_motorbike(motorbike, person_locations)
    if len(list_related_person) == 0:
        # print("Reject error: Ko co nguoi tren xe")
        return False

    img_crop = frame[motorbike.top:motorbike.bottom, motorbike.left:motorbike.right]
    if not check_license_plate(img_crop):
        print("xe di nguoc chieu")
        return False
    return True


def check_valid_left_motorbike(list_position=[], motorbike='', horizontalLine=LinePoint(0, 0, 0, 0),
                               lowerBound=LinePoint(0, 0, 0, 0), leftBound=LinePoint(0, 0, 0, 0),
                               rightBound=LinePoint(0, 0, 0, 0)):
    if motorbike.right < min(leftBound.left, leftBound.right):
        # print("Reject error: Xe vuot can trai")
        return False

    if motorbike.left > max(rightBound.left, rightBound.right):
        # print("Reject error: Xe vuot can phai")
        return False

    person_locations = list_position['PersonLocation']
    list_related_person = related_people_to_motorbike(motorbike, person_locations)
    if len(list_related_person) == 0:
        # print("Reject error: Ko co nguoi tren xe")
        return False

    # duoi can duoi
    if motorbike.top > max(lowerBound.top, lowerBound.bottom):
        return False

    return True
