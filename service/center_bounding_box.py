from location.CenterPoint import CenterPoint

def calculateCenterOfBoundingBox(left, top, right, bottom):
    x_center = (right + left)/2
    y_center = (top + bottom)/2
    return CenterPoint(x_center, y_center)