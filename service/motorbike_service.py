import sys

import numpy as np

from case_resolve import *
from database_access import *
from location.LinePoint import *
from location.Point import Point
from service.helmet_service import related_helmet_to_people
from service.helmet_service import related_people_to_motorbike
from service.license_plate_service import related_motorbike_to_license
from service.lane_service import is_left_or_above


def detect_passing_red_light(motorbike, line, lane_distance):
    rate = 0.0
    if (line.left < motorbike.left < line.right) or (line.left < motorbike.right < line.right):
        motorbike_lower_point = Point((motorbike.left + motorbike.right) / 2, motorbike.bottom)
        if is_left_or_above(line, motorbike_lower_point):
            p1 = np.array([line.left, line.top])
            p2 = np.array([line.right, line.bottom])
            p3 = np.array([(motorbike.left + motorbike.right) / 2, motorbike.bottom])
            distance = np.cross(p2 - p1, p3 - p1) / np.linalg.norm(p2 - p1)

            # print("passing red distance = ", distance)
            rate = abs(distance) * 100 / lane_distance
            # print("distance = ", distance, " target distance = ", lane_distance)
    return rate


def detect_crossing(motorbike, horizontalLine):
    rate = 0.0
    motorbike_upper_point = Point((motorbike.left + motorbike.right) / 2, motorbike.top)
    motorbike_lower_point = Point((motorbike.left + motorbike.right) / 2, motorbike.bottom)

    if is_left_or_above(horizontalLine, motorbike_upper_point) and \
            not is_left_or_above(horizontalLine, motorbike_lower_point):
        # linear equations of horizontal line
        leftPoint = Point(horizontalLine.left, horizontalLine.top)
        rightPoint = Point(horizontalLine.right, horizontalLine.bottom)

        # left line
        motoLeftUpper = Point(motorbike.left, motorbike.top)
        motoLeftLower = Point(motorbike.left, motorbike.bottom)

        # target distance
        targetDist = motoLeftLower.calculateDistance(motoLeftUpper)

        # right line
        motoRightUpper = Point(motorbike.right, motorbike.top)
        motoRightLower = Point(motorbike.right, motorbike.bottom)

        # intersection points
        leftIntersec = Point.pointIntersection(leftPoint, rightPoint, motoLeftUpper, motoLeftLower)
        rightIntersec = Point.pointIntersection(leftPoint, rightPoint, motoRightUpper, motoRightLower)

        dis1 = motoLeftUpper.calculateDistance(leftIntersec)
        dis2 = motoRightUpper.calculateDistance(rightIntersec)

        rate = 100 * max(dis1, dis2) / targetDist
    return rate


def detect_encroaching(motorbike, linePoint):
    ratio = 0.0
    if motorbike.bottom <= (linePoint.top + 10):
        return ratio

    if (motorbike.left + motorbike.right)/2 > max(linePoint.left, linePoint.right):
        return ratio

    # location of marker
    upperMarker = Point(linePoint.left, linePoint.top)
    lowerMarker = Point(linePoint.right, linePoint.bottom)

    # print out
    # print("upper Marker: ", linePoint.left, " - ", linePoint.top)
    # print("lower Marker: ", linePoint.right, " - ", linePoint.bottom)

    # upper line
    leftUpLine = Point(motorbike.left, motorbike.top)
    rightUpLine = Point(motorbike.right, motorbike.top)

    # lower line
    leftLowLine = Point(motorbike.left, motorbike.bottom)
    rightLowLine = Point(motorbike.right, motorbike.bottom)

    # distance of bounding box
    targetDistance = leftUpLine.calculateDistance(rightUpLine)
    # print("target horizontal = ", targetDistance)

    intersect1 = Point.pointIntersection(upperMarker, lowerMarker, leftUpLine, rightUpLine)
    # print("intersect1 : ", intersect1.x, " - ", intersect1.y)

    if intersect1.x == sys.float_info.max and intersect1.y == sys.float_info.max:
        calculateDis1 = 0
    if intersect1.x > rightUpLine.x:
        calculateDis1 = targetDistance
    if intersect1.x < leftUpLine.x:
        calculateDis1 = 0
    if (intersect1.x >= leftUpLine.x) and (intersect1.x <= rightUpLine.x):
        # calculate distance from the left to intersection point
        calculateDis1 = leftUpLine.calculateDistance(intersect1)
        # print("calculate dis 1 =", calculateDis1)

        # intersection of lower line and marker line
    intersect2 = Point.pointIntersection(upperMarker, lowerMarker, leftLowLine, rightLowLine)
    # print("intersect2 : ", intersect2.x, " - ", intersect2.y)

    calculateDis2 = 0
    if intersect2.x == sys.float_info.max and intersect2.y == sys.float_info.max:
        calculateDis2 = 0
    if intersect2.x > rightLowLine.x:
        calculateDis2 = targetDistance
    if intersect2.x < leftLowLine.x:
        calculateDis2 = 0
    if (intersect2.x >= leftLowLine.x) and (intersect2.x <= rightLowLine.x):
        calculateDis2 = leftLowLine.calculateDistance(intersect2)
        # print("calculate dis 2 =", calculateDis2)

    # print("max dis = ", max(calculateDis1, calculateDis2))
    ratio = 100 * max(calculateDis1, calculateDis2) / targetDistance
    # print("encrossing ratio = ", ratio)
    return ratio


# a,b is the 2 points of line, c is the point
def is_left(a, b, c):
    if ((b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x)) > 0:
        return 1
    if ((b.x - a.x) * (c.y - a.y) - (b.y - a.y) * (c.x - a.x)) < 0:
        return -1
    return 0


def detect_not_wearing_helmet(motorbike, people, helmets):
    count_wearing = 0
    # print("people in ham detect not wearing helmet ")
    # for ex_object in people:
    #     print("PERSON ham detect not wearing NE; ", "left : ", ex_object.left, " - ", "top: ", ex_object.top, " - ",
    #           "right: ",
    #           ex_object.right, " - ", " bottom: ", ex_object.bottom)
    list_related_people = related_people_to_motorbike(motorbike, people)

    for related_person in list_related_people:
        is_related_helmet = related_helmet_to_people(related_person, helmets)
        if is_related_helmet:
            count_wearing += 1
    if count_wearing >= len(list_related_people):
        return 0.0
    else:
        return 100.0


def detect_wrong_lane(list_position=[], horizontalLine=LinePoint(0, 0, 0, 0), upperBound=LinePoint(0, 0, 0, 0),
                      verticalLine=LinePoint(0, 0, 0, 0), frame=0, leftBound=LinePoint(0, 0, 0, 0),
                      rightBound=LinePoint(0, 0, 0, 0), camera_id=0):
    location = load_line(camera_id)

    motorbike_locations = list_position['MotorbikeLocation']
    license_location = list_position['LicenseLocation']
    person_locations = list_position['PersonLocation']
    helmet_locations = list_position['HelmetLocation']

    result = []

    upperLimit = Point((leftBound.left + leftBound.right) / 2, (leftBound.top + leftBound.bottom) / 2)

    for license in license_location:
        if (license.x > leftBound.left) and (upperBound.top > license.y > upperLimit):
            # insert wrong lane
            img_crop = frame[license.top: license.bottom, license.left:license.right]
            license_number = get_license_number(img_crop)
            file_name = '5' + str(get_time()) + '.jpg'
            storage_picture = draw_motorbike(
                motorbike.frame, motorbike.left, motorbike.top, motorbike.right, motorbike.bottom)
            storage_picture = put_Text(storage_picture, motorbike.left, motorbike.top, 'wrong lane')
            # storage_picture = draw_bound(storage_picture, list_bounding)
            cv2.imwrite(file_name, storage_picture)
            image_url = save('5' + file_name, file_name)
            image_id = insert_image(image_url, camera_id)
            print("co loi wrong lane ne - ", image_id)
            insert_case(5, location, license_number, image_id)

            motorbike = related_motorbike_to_license(license, motorbike_locations)
            if motorbike != '':
                rate = detect_not_wearing_helmet(motorbike, person_locations, helmet_locations)
                if rate == 100:
                    # insert not wearing helmet
                    img_crop = frame[license.top: license.bottom, license.left:license.right]
                    license_number = get_license_number(img_crop)
                    file_name = '4' + str(get_time()) + '.jpg'
                    storage_picture = draw_motorbike(
                        motorbike.frame, motorbike.left, motorbike.top, motorbike.right, motorbike.bottom)
                    storage_picture = put_Text(storage_picture, motorbike.left, motorbike.top, 'not wearing helmet')
                    # storage_picture = draw_bound(storage_picture, list_bounding)
                    cv2.imwrite(file_name, storage_picture)
                    image_url = save('4' + file_name, file_name)
                    image_id = insert_image(image_url, camera_id)
                    print("co loi wrong lane ne - ", image_id)
                    insert_case(5, location, license_number, image_id)
    return True
