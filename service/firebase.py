import pyrebase

config = {
    "apiKey": "AIzaSyBYp_ihugt_q-w1_M1eLcqpvsemXq2RH_o",
    "authDomain": "capstone-dtv.firebaseapp.com",
    "databaseURL": "https://capstone-dtv.firebaseio.com",
    "projectId": "capstone-dtv",
    "storageBucket": "capstone-dtv.appspot.com",
    "messagingSenderId": "19340638447",
    "appId": "1:19340638447:web:204777d5f920179c0cc31a",
    "measurementId": "G-3TX5HC3QCL"
}

def save(img_name_firebase, img_path_local):
    firebase = pyrebase.initialize_app(config)
    storage = firebase.storage()
    storage.child('images/' + img_name_firebase).put(img_path_local)
    url = storage.child('images/'+img_name_firebase).get_url(None)
    return url

