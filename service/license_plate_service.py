from yolov3.configs import *
from yolov3.utils import Load_Yolo_model, detect_image_v2
from utils.geometric_util import calculateOverlapRate

yolo_license = Load_Yolo_model(type='license')


def check_license_plate(img):
    license_location = detect_image_v2(yolo_license, img, input_size=YOLO_INPUT_SIZE,
                                    CLASSES=YOLO_LICENSE_CLASS, rectangle_colors=(255, 0, 0), helmet_flag=False, traffic_flag=False)['LicenseLocation']
    return license_location


def related_motorbike_to_license(license, motorbikes):
    maxOverlap = 0
    motorOverlap = ''
    for motorbike in motorbikes:
        overlapRate = calculateOverlapRate(license, motorbike)
        # print("overlap = ", overlapRate)
        if overlapRate > maxOverlap:
            maxOverlap = overlapRate
            motorOverlap = motorbike

    return motorOverlap
