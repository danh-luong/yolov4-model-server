import numpy as np


def calculate_distance(line, bound):
    p1 = np.array([line.left, line.top])
    p2 = np.array([line.right, line.bottom])
    p3 = np.array([(bound.left + bound.right) / 2, (bound.bottom + bound.top) / 2])
    distance = np.cross(p2 - p1, p3 - p1) / np.linalg.norm(p2 - p1)
    return abs(distance)


def is_left_or_above_CT(line, point):
    print("trong so = ",
          (line.right - line.left) * (point.y - line.top) - (line.bottom - line.top) * (point.x - line.left))
    return ((line.right - line.left) * (point.y - line.top) - (line.bottom - line.top) * (point.x - line.left)) >= 0


def is_left_or_above(line, point):
    if point.y <= (line.top + line.bottom) / 2:
        return True
    return False
