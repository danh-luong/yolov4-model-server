from location.Point import Point
from utils.BusinessConstants import BusinessConstants
from location.LinePoint import LinePoint
from location.Point import Point
import sys

def listEncroaching(motorbikeLocationList, linePoint):
    listResult = []
    #location of marker
    upperMarker = Point(linePoint.left, linePoint.top)
    lowerMarker = Point(linePoint.right, linePoint.bottom)
    
    for motorbikeLocation in motorbikeLocationList:
        leftUpLine = Point(motorbikeLocation.left, motorbikeLocation.top)
        rightUpLine = Point(motorbikeLocation.right, motorbikeLocation.bottom)

        #distance of bounding box
        targetDistance = leftUpLine.calculateDistance(rightUpLine)

        #intersection of upper line and marker line
        intersect1 = Point.pointIntersection(upperMarker, lowerMarker, leftUpLine, rightUpLine)

        if (intersect1.x == sys.float_info.max and intersect1.y == sys.float_info.max):
            calculateDis1 = 0
        else:
            #calculate distance from the left to intersection point
            calculateDis1 = leftUpLine.calculateDistance(intersect1)

            #lower line
            leftLowLine = Point(motorbikeLocation.left, motorbikeLocation.top)
            rightLowLine = Point(motorbikeLocation.right, motorbikeLocation.bottom)
            
            #intersection of lower line and marker line
            intersect2 = Point.pointIntersection(upperMarker, lowerMarker, leftLowLine, rightLowLine)

            if (intersect2.x == sys.float_info.max and intersect2.y == sys.float_info.max):
                calculateDis2 = 0
            else:
                calculateDis2 = leftLowLine.calculateDistance(intersect2)
                ratio = max([calculateDis1, calculateDis2]) / targetDistance
                if (ratio >= BusinessConstants.thresholdEncroaching):
                    listResult.append(motorbikeLocation)
    return listResult