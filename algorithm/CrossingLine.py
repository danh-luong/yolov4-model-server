from location.Point import Point
from utils.BusinessConstants import BusinessConstants
from location.LinePoint import LinePoint

def listCrossing(motorbikeLocationList, linePoint):
    listResult = []
    for motorbikeLocation in motorbikeLocationList:
        upperPoint = Point(motorbikeLocation.left, motorbikeLocation.top)
        lowerPoint = Point(motorbikeLocation.right, motorbikeLocation.bottom)

        distanceRect = upperPoint.calculateDistance(lowerPoint)
        intersectPoint = Point(motorbikeLocation.left, linePoint.top)

        crossingDistance = upperPoint.calculateDistance(intersectPoint)
        ratio = 1.0 * crossingDistance / distanceRect

        if(ratio > BusinessConstants.thresholdCrossing):
            listResult.append(motorbikeLocation)
    return listResult