import concurrent.futures
import threading
import time
import copy

from threading import Lock
from numpy import *

from aggregateViolations import process
from service.draw import draw_line, draw
from service.logic import *
from test_package.print_frame import print_out_violation_picture
from yolov3.configs import *
from yolov3.utils import detect_image, Load_Yolo_model

lock = Lock()
listGreenPosition = {}
listRedPosition = {}
arr = []
countRed = 0
countGreen = 0
totalFrame = 0
startGreenFrame = 0
startRedFrame = 0
redFrame = 0
green_frame = 0
red_len = 0
green_len = 0


def light_status(traffic_light):
    if len(traffic_light) > 0:
        for light in traffic_light:
            is_red = getattr(light, 'red')
            if is_red:
                return True
    return False


def increment(name):
    global countRed
    global countGreen
    global totalFrame
    global redFrame
    global green_frame
    global green_len
    global red_len

    if name == 'countRed':
        countRed += 1
        return countRed
    elif name == 'countGreen':
        countGreen += 1
        return countGreen
    elif name == 'totalFrame':
        totalFrame += 1
        return totalFrame
    elif name == 'redFrame':
        redFrame += 1
        return redFrame
    elif name == 'green_frame':
        green_frame += 1
        return green_frame
    elif name == 'red_len':
        red_len += 1
        return red_len
    elif name == 'green_len':
        green_len += 1
        return green_len


def detect_violation_frame(arr, position, horizontalLine, upper_bound, verticalLine, left_bound, right_bound,
                           total_frame, camera_id, location):
    global listRedPosition
    global countRed
    global countGreen
    global listGreenPosition
    global totalFrame
    global startGreenFrame
    global startRedFrame
    global redFrame
    global green_frame
    global green_len
    global red_len

    list_bounding = [upper_bound, left_bound, right_bound]
    tempArr = arr.copy()
    storage_frame = tempArr[1]
    original_image = copy.copy(storage_frame)
    tempList = detect_image(tempArr[0], original_image, tempArr[2], tempArr[3],
                            tempArr[4], tempArr[5], tempArr[6], tempArr[7], tempArr[8])
    traffic_light = tempList['TrafficLight']
    motorbike_list = tempList['MotorbikeLocation']
    helmet_list = tempList['HelmetLocation']
    people_list = tempList['PersonLocation']
    # if len(motorbike_list) > 0:
    #     for i in motorbike_list:
    #         storage_frame = draw(
    #             storage_frame, i.left, i.top, i.right, i.bottom)
    # if len(traffic_light) > 0:
    #     for i in traffic_light:
    #         storage_frame = draw(
    #             storage_frame, i.left, i.top, i.right, i.bottom)
    # if len(helmet_list) > 0:
    #     for i in helmet_list:
    #         storage_frame = draw(
    #             storage_frame, i.left, i.top, i.right, i.bottom)
    # if len(people_list) > 0:
    #     for i in people_list:
    #         storage_frame = draw(
    #             storage_frame, i.left, i.top, i.right, i.bottom)
    lock.acquire()
    is_red = light_status(traffic_light)
    if position == "right":
        if is_red:
            count_red_local = 0
            # lock.acquire()
            count_red_local = increment('countRed')
            if count_red_local == 1:
                redFrame = total_frame
                startRedFrame = total_frame
            else:
                increment('redFrame')
            if countGreen < 3:
                if countGreen > 0:
                    len_tmp = len(listGreenPosition)
                    for j in range(0, countGreen):
                        increment('countRed')
                        increment('redFrame')
                    if len_tmp > 0:
                        for i in range(1, (len_tmp + 1)):
                            increment('red_len')
                            listRedPosition[red_len] = listGreenPosition[i]
                        listGreenPosition.clear()
                    countGreen = 0
                    green_frame = 0
                    startGreenFrame = 0
                    green_len = 0
            # lock.release()
            violations = detect_right_violation(
                tempList, horizontalLine, upper_bound, verticalLine, original_image, left_bound, right_bound)
            # cv2.imwrite('C:\\Users\\ChienDinh\\Desktop\\code\\python-be\\imgs\\red' +
            #             str(total_frame) + '.jpg', storage_frame)
            if len(violations) > 0:
                # lock.acquire()
                # print_out_violation_picture(
                #     violations, list_bounding, storage_frame)
                increment('red_len')
                listRedPosition[red_len] = violations
                # lock.release()
        else:
            count_green_local = 0
            # lock.acquire()
            count_green_local = increment('countGreen')
            if count_green_local == 1:
                green_frame = total_frame
                startGreenFrame = total_frame
            else:
                increment('green_frame')
            if countRed < 3:
                if countRed > 0:
                    len_tmp = len(listRedPosition)
                    for j in range(0, countRed):
                        increment('countGreen')
                        increment('green_frame')
                    if len_tmp > 0:
                        for i in range(1, (len_tmp + 1)):
                            increment('green_len')
                            listGreenPosition[green_len] = listRedPosition[i]
                        listRedPosition.clear()
                    countRed = 0
                    redFrame = 0
                    startRedFrame = 0
                    red_len = 0
            # lock.release()
            violations = detect_right_green_violation(
                tempList, horizontalLine, upper_bound, verticalLine, original_image, left_bound, right_bound)
            # cv2.imwrite('C:\\Users\\ChienDinh\\Desktop\\code\\python-be\\imgs\\green' +
            #             str(total_frame) + '.jpg', storage_frame)
            if len(violations) > 0:
                # lock.acquire()
                # print_out_violation_picture(
                #     violations, list_bounding, storage_frame)
                increment('green_len')
                listGreenPosition[green_len] = violations
                # lock.release()
        final_process = {}
        if len(listRedPosition) != 0:
            if startGreenFrame == (redFrame + 1) and countGreen > 2:
                # lock.acquire()
                final_process = process(listRedPosition)
                listRedPosition.clear()
                countRed = 0
                totalFrame = green_frame
                redFrame = 0
                startGreenFrame = 0
                startRedFrame = 0
                red_len = 0
                # lock.release()
        if len(listGreenPosition) != 0:
            if startRedFrame == (green_frame + 1) and countRed > 2:
                # lock.acquire()
                final_process = process(listGreenPosition)
                listGreenPosition.clear()
                countGreen = 0
                totalFrame = redFrame
                green_frame == 0
                startRedFrame = 0
                green_len = 0
                startGreenFrame = 0
                # lock.release()
        if len(final_process) > 0:
            for object in final_process:
                print("-----")
                print("FINAL RESULT")
                print("top: ", object.top, " - left: ", object.left,
                      "- bottom: ", object.bottom, " - right: ", object.right)
                print("x = ", object.x)
                print("y = ", object.y)
                print("vio_pas_rate = ",
                      object.vio_pas_rate)
                print("vio_cro_rate = ",
                      object.vio_cro_rate)
                print("vio_enc_rate = ",
                      object.vio_enc_rate)
                print("vio_hel_rate = ",
                      object.vio_hel_rate)
                conduct_case(
                    camera_id, object, location, verticalLine, horizontalLine, list_bounding)
    else:
        violations = detect_left_violation(tempList, horizontalLine, upper_bound, verticalLine, original_image,
                                           left_bound,
                                           right_bound)
    lock.release()
    return 0


def process_each_frame(camera, yolo):
    camera_id = camera[0]
    location = camera[1]
    connection = camera[2]
    status = camera[3]
    position = camera[4]
    if status == "active":
        cap = cv2.VideoCapture(connection)
        # cap = cv2.VideoCapture('rtsp://admin:palexy802@192.168.43.9:5544/Streaming/channels/101')
        fps = int(cap.get(cv2.CAP_PROP_FPS))
        # fps = math.floor(fps/3)
        line_list = load_line(camera_id)
        horizontalLine = LinePoint(1117, 905, 1845, 899)
        verticalLine = LinePoint(1117, 905, 951, 1501)
        upper_bound = LinePoint(900, 700, 1901, 731)
        left_bound = LinePoint(900, 700, 650, 1509)
        right_bound = LinePoint(1901, 731, 2039, 1501)
        list_bounding = [upper_bound, left_bound, right_bound]
        global arr
        global totalFrame
        count = 0
        while (cap.isOpened()):
            ret, frame = cap.read()
            count += 1
            if count % fps == 0:
                if ret == True:
                    # total_frame = 0
                    lock.acquire()
                    increment('totalFrame')
                    lock.release()
                    print('total_frame: ', totalFrame)
                    # goi thread ra de tra ve violation cua frame do
                    # draw_line(frame, horizontalLine.left, horizontalLine.top,
                    #           horizontalLine.right, horizontalLine.bottom)
                    # draw_line(frame, verticalLine.left, verticalLine.top,
                    #           verticalLine.right, verticalLine.bottom)
                    # draw_line(frame, upper_bound.left,
                    #           upper_bound.top, upper_bound.right, upper_bound.bottom)
                    # draw_line(frame, left_bound.left,
                    #           left_bound.top, left_bound.right, left_bound.bottom)
                    # draw_line(frame, right_bound.left,
                    #           right_bound.top, right_bound.right, right_bound.bottom)
                    # lock.acquire()
                    original_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                    original_image = cv2.cvtColor(
                        original_image, cv2.COLOR_BGR2RGB)
                    arr = [yolo, original_image, 416, YOLO_COCO_CLASSES,
                           0.4, 0.4, '', True, True]
                    thread = threading.Thread(target=detect_violation_frame, args=(
                        arr, position, horizontalLine, upper_bound, verticalLine, left_bound, right_bound, totalFrame,
                        camera_id, location))
                    thread.start()
                    # lock.release()
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
        cap.release()


if __name__ == '__main__':
    yolo = Load_Yolo_model()
    # load each camera
    camera = [1, 2, "C:\\Users\\ChienDinh\\Desktop\\other\\test-mgs\\videos\\test_1.mp4", "active",
              "right"]
    # camera = [1, 2, "C:\\Users\\Vinh Luu\\Downloads\\V3.mov", "active",
    #           "right"]

    link = "C:\\Users\\Vinh Luu\\Downloads\\2020-20201122T060303Z-001\\21-11-2020\\test_2.mp4"
    camera = [1, 2, link, "active", "right"]
    process_each_frame(camera, yolo)
    # list_camera = retrieve_camera()
    # for camera in list_camera:
    #     # start threading
    #     thread = threading.Thread(target=process_each_frame, args=(camera,))
    #     thread.daemon = True
    #     thread.start()
    while True:
        time.sleep(1)
