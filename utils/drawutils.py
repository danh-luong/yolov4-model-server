import cv2
import copy


def draw_motorbike(image, left, top, right, bottom):
    img = copy.copy(image)
    cv2.rectangle(img, (left, top), (right, bottom), (50, 50, 240), 2)
    return img


def draw_object_with_text(image, left, top, right, bottom, text):
    img = copy.copy(image)
    img = cv2.rectangle(img, (left, top), (right, bottom), (50, 50, 240), 2)
    org = (left + 10, top - 15)
    color = (36, 255, 12)
    image = cv2.putText(img, text, org, cv2.FONT_HERSHEY_SIMPLEX, 0.9, color, 2)
    return image


def draw_line(image, line, is_verticle):
    img = copy.copy(image)
    # represents the bottom right corner of image
    start_point = (line.left, line.top)
    end_point = (line.right, line.bottom)
    color = (255, 153, 255)
    # Green color in BGR
    if is_verticle:
        color = (0, 255, 0)
    # Line thickness of 9 px
    thickness = 8
    if is_verticle:
        thickness = 9
    # Using cv2.line() method
    # Draw a diagonal green line with thickness of 9 px
    final_image = cv2.line(img, start_point, end_point, color, thickness)
    return final_image


def put_Text(inserted_image, left, top, text):
    image = copy.copy(inserted_image)
    org = (left + 10, top - 15)
    color = (36, 255, 12)
    # color = (0, 0, 255)
    image = cv2.putText(image, text, org, cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 1)
    return image


def draw_bound(inserted_image, list_bounding):
    image = copy.copy(inserted_image)
    color = (255, 0, 0)
    thickness = 3
    for line in list_bounding:
        start_point = (line.left, line.top)
        end_point = (line.right, line.bottom)
        image = cv2.line(image, start_point, end_point, color, thickness)
    return image
