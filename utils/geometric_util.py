import math
from location.Point import Point


def dist(p1, p2):
    return math.sqrt((p1.x - p2.x) * (p1.x - p2.x) + (p1.y - p2.y) * (p1.y - p2.y))


def calculateArea(object):
    return (object.right - object.left) * (object.bottom - object.top)


def calculateDistance(object):
    point1 = Point(object.left, object.top)
    point2 = Point(object.right, object.bottom)
    return dist(point1, point2)


def calculateOverlapRate(object1, object2):
    if (object1.right <= object2.left) or (object1.left >= object2.right):
        return 0
    if (object1.top >= object2.bottom) or (object1.bottom <= object2.top):
        return 0

    length = min(object1.right, object2.right) - max(object1.left, object2.left)
    width = min(object1.bottom, object2.bottom) - max(object1.top, object2.top)

    overlaparea = length * width
    # print("overlap area = ", overlaparea)
    minarea = min(calculateArea(object1), calculateArea(object2))
    # print("minarea = ", minarea)

    # print("OVERLAP STATISTIC")
    # print("object 1 = ", object1.left, ",", object1.top, ",", object1.right, ",", object1.bottom)
    # print("object 2 = ", object2.left, ",", object2.top, ",", object2.right, ",", object2.bottom)

    # print("overlap area = ", overlaparea)
    # print("min area =", minarea)
    # print("overlap rate = ", overlaparea / minarea)

    return overlaparea / minarea
