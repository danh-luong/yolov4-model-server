import concurrent.futures
import threading
import time
from multiprocessing import Process, Queue

from numpy import *
import numpy as np
import copy
import tensorflow as tf

from aggregateViolations import process
from service.draw import draw_line, draw
from service.logic import *
from location.TrafficLight import TrafficLight
from test_package.print_frame import print_out_violation_picture, print_frame_process, print_raw_frame, print_out_violation_picture_light, print_detect_frame
from yolov3.configs import *
from yolov3.yolov4 import *
from yolov3.utils import detect_image, Load_Yolo_model, image_preprocess, nms, postprocess_boxes, draw_bbox, is_red
from thread_test import light_status
from tensorflow.python.saved_model import tag_constants

yolo = Load_Yolo_model()
countRed = 0
countGreen = 0
totalFrame = 0
startGreenFrame = 0
startRedFrame = 0
redFrame = 0
green_frame = 0
red_len = 0
green_len = 0
listRedPosition = {}
listGreenPosition = {}
original_frames = Queue()
frames_data = Queue()


def increment(name):
    global countRed
    global countGreen
    global totalFrame
    global redFrame
    global green_frame
    global green_len
    global red_len

    if name == 'countRed':
        countRed += 1
    elif name == 'countGreen':
        countGreen += 1
    elif name == 'totalFrame':
        totalFrame += 1
    elif name == 'redFrame':
        redFrame += 1
    elif name == 'green_frame':
        green_frame += 1
    elif name == 'red_len':
        red_len += 1
    elif name == 'green_len':
        green_len += 1


def detect_image(Yolo, input_size=416, CLASSES=YOLO_COCO_CLASSES, score_threshold=0.4, iou_threshold=0.4, rectangle_colors='', helmet_flag=True, traffic_flag=True, camera_id = 1, location ='', verticalLine = LinePoint(0,0,0,0), horizontalLine = LinePoint(0,0,0,0), list_bounding = []):
    global listRedPosition
    global countRed
    global countGreen
    global listGreenPosition
    global totalFrame
    global startGreenFrame
    global startRedFrame
    global redFrame
    global green_frame
    global green_len
    global red_len
    global original_frames
    global frames_data
    while True:
        pred_bbox = []
        if original_frames.qsize() > 0:
            image = original_frames.get()
            original_image = copy.copy(image)
            print_raw_frame(original_image)
            if frames_data.qsize() > 0:
                image_data = frames_data.get()
                if YOLO_FRAMEWORK == "tf":
                    increment('totalFrame')
                    print('total frame: ' + str(totalFrame))
                    pred_bbox = Yolo.predict(image_data)

                    pred_bbox = [tf.reshape(x, (-1, tf.shape(x)[-1]))
                                 for x in pred_bbox]
                    pred_bbox = tf.concat(pred_bbox, axis=0)

                    bboxes = postprocess_boxes(
                        pred_bbox, original_image, input_size, score_threshold)
                    bboxes = nms(bboxes, iou_threshold, method='nms')
                    tempList = draw_bbox(
                        original_image, bboxes, CLASSES=CLASSES, rectangle_colors=rectangle_colors, helmet_flag=helmet_flag,
                        traffic_flag=traffic_flag)
                    traffic_light = tempList['TrafficLight']
                    motorbike_list = tempList['MotorbikeLocation']
                    helmet_list = tempList['HelmetLocation']
                    people_list = tempList['PersonLocation']
                    if len(traffic_light) == 0:
                         traffic_light.append(TrafficLight(
                                            int(1727), int(269), int(1801), int(393),
                                            is_red(int(393), int(269), int(1727), int(1801), original_image)))
                    storage_frame = copy.copy(original_image)
                    if len(motorbike_list) > 0:
                        for i in motorbike_list:
                            storage_frame = draw(
                                storage_frame, i.left, i.top, i.right, i.bottom)
                    print_detect_frame(storage_frame)
                    if len(traffic_light) > 0:
                        for i in traffic_light:
                            storage_frame = draw(
                                storage_frame, i.left, i.top, i.right, i.bottom)
                    # if len(helmet_list) > 0:
                    #     for i in helmet_list:
                    #         storage_frame = draw(
                    #             storage_frame, i.left, i.top, i.right, i.bottom)
                    # if len(people_list) > 0:
                    #     for i in people_list:
                    #         storage_frame = draw(
                    #             storage_frame, i.left, i.top, i.right, i.bottom)
                    red = light_status(traffic_light)
        
                    if red:
                        increment('countRed')
                        if countRed == 1:
                            redFrame = totalFrame
                            startRedFrame = totalFrame
                        else:
                            increment('redFrame')
                        if countGreen < 3:
                            if countGreen > 0:
                                len_tmp = len(listGreenPosition)
                                for j in range(0, countGreen):
                                    increment('countRed')
                                    increment('redFrame')
                                if len_tmp > 0:
                                    for i in range(1, (len_tmp + 1)):
                                        increment('red_len')
                                        listRedPosition[red_len] = listGreenPosition[i]
                                    listGreenPosition.clear()
                                countGreen = 0
                                green_frame = 0
                                startGreenFrame = 0
                                green_len = 0
                        violations = detect_right_violation(
                            tempList, horizontalLine, list_bounding[0], verticalLine, original_image, list_bounding[1], list_bounding[2])
                        if len(violations) > 0:
                            increment('red_len')
                            print_out_violation_picture_light(
                                violations, list_bounding, original_image, is_red)
                            listRedPosition[red_len] = violations
                    else:
                        increment('countGreen')
                        if countGreen == 1:
                            green_frame = totalFrame
                            startGreenFrame = totalFrame
                        else:
                            increment('green_frame')
                        if countRed < 3:                       
                            if countRed > 0:
                                len_tmp = len(listRedPosition)
                                for j in range(0, countRed):
                                    increment('countGreen')
                                    increment('green_frame')
                                if len_tmp > 0:
                                    for i in range(1, (len_tmp + 1)):
                                        increment('green_len')
                                        listGreenPosition[green_len] = listRedPosition[i]
                                    listRedPosition.clear()
                                countRed = 0
                                redFrame = 0
                                startRedFrame = 0
                                red_len = 0
                        violations = detect_right_green_violation(
                            tempList, horizontalLine, list_bounding[0], verticalLine, original_image, list_bounding[1], list_bounding[2])
                        if len(violations) > 0:
                            print_out_violation_picture_light(
                                violations, list_bounding, original_image, is_red)
                            increment('green_len')
                            listGreenPosition[green_len] = violations

                final_process = {}
                if len(listRedPosition) != 0:
                    if startGreenFrame == (redFrame + 1) and countGreen > 2:
                        final_process = process(listRedPosition)
                        listRedPosition.clear()
                        countRed = 0
                        totalFrame = green_frame
                        redFrame = 0
                        startGreenFrame = 0
                        startRedFrame = 0
                        red_len = 0
                if len(listGreenPosition) != 0:
                    if startRedFrame == (green_frame + 1) and countRed > 2:
                        final_process = process(listGreenPosition)
                        listGreenPosition.clear()
                        countGreen = 0
                        totalFrame = redFrame
                        green_frame == 0
                        startRedFrame = 0
                        green_len = 0
                        startGreenFrame = 0
                if len(final_process) > 0:
                    print_frame_process(final_process)
                    for object in final_process:
                        print("-----")
                        print("FINAL RESULT")
                        print("top: ", object.top, " - left: ", object.left,
                            "- bottom: ", object.bottom, " - right: ", object.right)
                        print("x = ", object.x)
                        print("y = ", object.y)
                        print("vio_pas_rate = ",
                            object.vio_pas_rate)
                        print("vio_cro_rate = ",
                            object.vio_cro_rate)
                        print("vio_enc_rate = ",
                            object.vio_enc_rate)
                        print("vio_hel_rate = ",
                            object.vio_hel_rate)
                        conduct_case(
                            camera_id, object, location, verticalLine, horizontalLine, list_bounding)


def process_each_frame(camera):
    camera_id = camera[0]
    location = camera[1]
    connection = camera[2]
    status = camera[3]
    position = camera[4]
    global totalFrame
    if status == "active":
        if position == 'right':
            cap = cv2.VideoCapture(connection)
            # cap = cv2.VideoCapture('rtsp://admin:palexy802@192.168.43.9:5544/Streaming/channels/101')
            fps = int(cap.get(cv2.CAP_PROP_FPS))
            # fps = math.floor(fps/3)
            line_list = load_line(camera_id)
            horizontalLine = LinePoint(1251, 881, 2019, 889)
            verticalLine = LinePoint(1251, 881, 1155, 1519)
            upper_bound = LinePoint(1815, 691, 1007, 695)
            left_bound = LinePoint(1007, 695, 775, 1509)
            right_bound = LinePoint(1815, 691, 2359, 1505)
            list_bounding = [upper_bound, left_bound, right_bound, horizontalLine, verticalLine]
            count = 0
            threading.Thread(target=detect_image, args=(
                yolo, 416, YOLO_COCO_CLASSES, 0.4, 0.4, '', True, True,
                camera_id, location, verticalLine, horizontalLine, list_bounding)).start()
            # process = Process(target=detect_image, args=(
            #     yolo, 416, YOLO_COCO_CLASSES, 0.4, 0.4, '', True, True,
            #     camera_id, location, verticalLine, horizontalLine, list_bounding))
            # process.start()
            while (cap.isOpened()):
                ret, frame = cap.read()
                count += 1
                if count % fps == 0:
                    if ret == True:                       
                        original_image = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
                        original_image = cv2.cvtColor(
                            original_image, cv2.COLOR_BGR2RGB)
                        original_frames.put(original_image)
                        image_data = image_preprocess(np.copy(original_image), [
                            416, 416])
                        image_data = image_data[np.newaxis, ...].astype(
                            np.float32)
                        frames_data.put(image_data)
                if cv2.waitKey(1) & 0xFF == ord('q'):
                    break
            # while True:
            #     if original_frames.qsize() == 0 and frames_data.qsize() == 0:
            #         process.terminate()
            #         break
        cap.release()


if __name__ == '__main__':
    # load each camera
    camera = [2, 2, "D:\\record-video\\test_10.mp4", "active",
              "right"]
    process_each_frame(camera)
    # list_camera = retrieve_camera()
    # for camera in list_camera:
    #     # start threading
    #     thread = threading.Thread(target=process_each_frame, args=(camera,))
    #     thread.daemon = True
    #     thread.start()
    while True:
        time.sleep(1)
