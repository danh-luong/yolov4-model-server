import threading

from case_resolve import *
from service.logic import *
from test_package.print_frame import *
from aggregateViolations import process

def draw_picture(image, tempList):
    # DRAW HINH LEN DE XEM
    motorbike_locations = tempList['MotorbikeLocation']
    license_location = tempList['LicenseLocation']
    person_locations = tempList['PersonLocation']
    helmet_locations = tempList['HelmetLocation']
    traffic_locations = tempList['TrafficLight']

    print(len(license_location), " ", len(traffic_light), " ", len(helmet_locations))

    paint_image = copy.copy(image)
    for ex_object in motorbike_locations:
        paint_image = draw_object_with_text(paint_image, ex_object.left, ex_object.top, ex_object.right,
                                            ex_object.bottom, "xe may")
    for ex_object in person_locations:
        paint_image = draw_object_with_text(paint_image, ex_object.left, ex_object.top, ex_object.right,
                                            ex_object.bottom, "nguoi")
    for ex_object in license_location:
        paint_image = draw_object_with_text(paint_image, ex_object.left, ex_object.top, ex_object.right,
                                            ex_object.bottom, "bien so xe")
    for ex_object in helmet_locations:
        paint_image = draw_object_with_text(paint_image, ex_object.left, ex_object.top, ex_object.right,
                                            ex_object.bottom, "mu bao hiem")

    for ex_object in traffic_locations:
        paint_image = draw_object_with_text(paint_image, ex_object.left, ex_object.top, ex_object.right,
                                            ex_object.bottom, "dengiaothong")
    file_name = "hinhxacdinhobject.jpg"
    cv2.imwrite(file_name, paint_image)
    image_url = save('1' + file_name, file_name)
    insert_image(image_url, 1)

    return True


camera_id = 5
location = "Test"
# connection = "C:\\Users\\Vinh Luu\\Downloads\\test1.jpg"
status = "active"
position = "right"

yolo = Load_Yolo_model()

if status == "active":

    horizontalLine = LinePoint(1125, 897, 1893, 897)
    verticalLine = LinePoint(1125, 897, 893, 1513)
    upper_bound = LinePoint(1119, 693, 1791, 739)
    left_bound = LinePoint(1119, 693, 889, 1513)
    right_bound = LinePoint(1791, 739, 2057, 1515)

    list_bounding = [upper_bound, left_bound, right_bound]
    list_draw_bounding = [upper_bound, left_bound, right_bound, horizontalLine, verticalLine]
    countRed = 0
    # INSERT CODE HERE
    camera_location = location
    is_red_trans = True
    listRedPosition = {}
    countTotal = 0
    list_aggre = {}

    link_1 = "C:\\Users\\Vinh Luu\\Downloads\\Frames-20201123T042024Z-002\\Frames\\test_6\\test_6 038.jpg"
    link_2 = "C:\\Users\\Vinh Luu\\Downloads\\Frames-20201123T042024Z-002\\Frames\\test_6\\test_6 039.jpg"

    link_3 = "C:\\Users\\Vinh Luu\\vio_enDownloads\\test_5-20201129T134410Z-001\\test_5\\test_5 037.jpg"
    link_4 = "C:\\Users\\Vinh Luu\\Downloads\\test_5-20201129T134410Z-001\\test_5\\test_5 038.jpg"

    folder = "C:\\Users\\Vinh Luu\\Downloads\\test_6-20201129T171109Z-001\\test_6\\test_6 042.jpg"
    link_5 = "C:\\Users\\Vinh Luu\\Downloads\\Test_Frame\\1.jpg"
    link_6 = "C:\\Users\\Vinh Luu\\Downloads\\Test_Frame\\2.jpg"
    link_7 = "C:\\Users\\Vinh Luu\\Downloads\\Test_Frame\\3.jpg"

    count = 0
    list_link = [link_5, link_6, link_7]
    for link in list_link:
        print("NEW FRAME")
        count += 1
        frame = cv2.imread(link)
        file_name = "hinhtoancanh.jpg"
        storage_picture = draw_bound(frame, list_draw_bounding)
        cv2.imwrite(file_name, storage_picture)
        image_url = save('1' + file_name, file_name)
        image_id = insert_image(image_url, camera_id)
        # frame = cv2.imread(connection)
        if position == "right":
            # cv2.imshow('a', frame)
            cv2.waitKey(0)
            arr = [yolo, frame, 416, YOLO_COCO_CLASSES, 0.4, 0.4, '', True]
            tempList = detect_image(yolo, frame, 416, YOLO_COCO_CLASSES, 0.4, 0.4, '', True, True)
            traffic_light = tempList['TrafficLight']

            # Ve Len Cho De Nhin
            copy_frame = copy.copy(frame)
            draw_picture(copy_frame, tempList)

            violations = detect_right_violation(
                tempList, horizontalLine, upper_bound, verticalLine, frame, left_bound, right_bound)

            if len(violations) > 0:
                list_aggre[count] = violations
                motorbike_number = 0
                for object in violations:
                    print("-----")
                    motorbike_number += 1
                    print("FINAL RESULT")
                    print("Motorbike number = ", motorbike_number)
                    print("top: ", object.top, " - left: ", object.left,
                          "- bottom: ", object.bottom, " - right: ", object.right)
                    print("x = ", object.x)
                    print("y = ", object.y)

                    print("vio_pas_rate = ", object.vio_pas_rate)
                    # print("pas left = ", object.pas_l)
                    print("vio_cro_rate = ", object.vio_cro_rate)
                    # print("cro left = ", object.cro_l)

                    print("vio_enc_rate = ", object.vio_enc_rate)
                    # print("enc left = ", object.enc_l)

                    print("vio_hel_rate = ", object.vio_hel_rate)
                    x = threading.Thread(
                        target=conduct_case(camera_id, object, camera_location, verticalLine, horizontalLine,
                                            list_bounding))
                    x.start()
                    print_out_violation_picture_light(
                        violations, list_bounding, copy_frame, True)
                    # conduct_case(camera_id, object, camera_location, verticalLine, horizontalLine,
                    #              list_bounding)
                    count_dif = 0
    if len(list_aggre) > 0:
        final_process = process(list_aggre)

cv2.destroyAllWindows()
