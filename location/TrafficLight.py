from location.ObjectLocation import *

class TrafficLight(ObjectLocation):
    def __init__(self, left, top, right, bottom, red):
        super().__init__(left, top, right, bottom)
        self.red = red   