import math
import sys

class Point: 

    def __init__(self, x, y):
        self.x = x
        self.y = y
    
    def calculateDistance(self, point):
        return math.sqrt((self.x - point.x) * (self.x - point.x) + (self.y - point.y) * (self.y - point.y))

    def pointIntersection(A, B, C, D):
        # Line AB represented as a1x + b1y = c1
        a1 = B.y - A.y
        b1 = A.x - B.x
        c1 = a1*(A.x) + b1*(A.y)

        # Line CD represented as a2x + b2y = c2
        a2 = D.y - C.y
        b2 = C.x - D.x
        c2 = a2*(C.x)+ b2*(C.y)

        determinant = a1*b2 - a2*b1 

        if (determinant == 0):
            return Point(sys.float_info.max, sys.float_info.max)
        else:
            x = (b2*c1 - b1*c2)/determinant
            y = (a1*c2 - a2*c1)/determinant
            return Point(x, y)