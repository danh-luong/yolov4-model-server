class ObjectLocation:
    def __init__(self, left, top, right, bottom):
        self.left = left
        self.top = top
        self.right = right
        self.bottom = bottom


def printObject(object):
    print("OBJECT")
    print("left = ", object.left)
    print("top = ", object.top)
    print("right = ", object.right)
    print("bottom = ", object.bottom)
