from location.ObjectLocation import *


class MotorbikeLocation(ObjectLocation):
    def __init__(self, left, top, right, bottom, x, y, vio_pas_rate, vio_cro_rate, vio_enc_rate, vio_hel_rate, count_hel,
                 vio_wro_rate, img_pas, img_cro, img_enc, img_hel, img_wro,
                 pas_l, pas_t, pas_r, pas_b,
                 cro_l, cro_t, cro_r, cro_b,
                 enc_l, enc_t, enc_r, enc_b,
                 hel_l, hel_t, hel_r, hel_b,
                 wro_l, wro_t, wro_r, wro_b
                 ):
        super().__init__(left, top, right, bottom)
        self.x = x
        self.y = y
        self.vio_pas_rate = vio_pas_rate
        self.vio_cro_rate = vio_cro_rate
        self.vio_enc_rate = vio_enc_rate
        self.vio_hel_rate = vio_hel_rate
        self.vio_wro_rate = vio_wro_rate
        self.img_pas = img_pas
        self.img_cro = img_cro
        self.img_enc = img_enc
        self.img_hel = img_hel
        self.img_wro = img_wro
        self.pas_l = pas_l
        self.pas_t = pas_t
        self.pas_r = pas_r
        self.pas_b = pas_b
        self.cro_l = cro_l
        self.cro_t = cro_t
        self.cro_r = cro_r
        self.cro_b = cro_b
        self.enc_l = enc_l
        self.enc_t = enc_t
        self.enc_r = enc_r
        self.enc_b = enc_b
        self.hel_l = hel_l
        self.hel_t = hel_t
        self.hel_r = hel_r
        self.hel_b = hel_b
        self.wro_l = wro_l
        self.wro_t = wro_t
        self.wro_r = wro_r
        self.wro_b = wro_b

        self.count_hel = count_hel


def printObject(object):
    print("----")
    print("left : ", object.left)
    print("right : ", object.right)
    print("top : ", object.top)
    print("bottom : ", object.bottom)

    print("x : ", object.x)
    print("y : ", object.y)

    print("vio_pas_rate = ", object.vio_pas_rate)
    print("vio_cro_rate = ", object.vio_cro_rate)
    print("vio_enc_rate = ", object.vio_enc_rate)
    print("vio_hel_rate = ", object.vio_hel_rate)
