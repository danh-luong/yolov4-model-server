from random import randrange
import random
from case_resolve import *

def print_out_violation_picture_light(violations, list_bounding, target_frame, is_red):
    # path = 'C:\\Users\\Vinh Luu\\Downloads\\Final_Result\\'
    path = 'D:\\TestDTVC\\Frame\\'

    # os.chdir(path)
    frame = copy.copy(target_frame)
    frame = draw_bound(frame, list_bounding)
    if is_red:
        frame = put_Text(frame, 300, 300, "RED")
    else:
        frame = put_Text(frame, 300, 300, "GREEN")
    for motorbike in violations:
        frame = draw_motorbike(frame, motorbike.left, motorbike.top, motorbike.right, motorbike.bottom)
        text = "pas: " + str(round(motorbike.vio_pas_rate, 2))
        frame = put_Text(frame, motorbike.left, motorbike.top - 60, text)

        text = "cro: " + str(round(motorbike.vio_cro_rate, 2))
        frame = put_Text(frame, motorbike.left, motorbike.top - 45, text)

        text = "enc: " + str(round(motorbike.vio_enc_rate, 2))
        frame = put_Text(frame, motorbike.left, motorbike.top - 30, text)

        text = "hel: " + str(round(motorbike.vio_hel_rate, 2))
        frame = put_Text(frame, motorbike.left, motorbike.top - 5, text)

    # cv2.imshow("anh xe", frame)
    # cv2.waitKey(0)
    image_name = path + str(get_time()) + str(random.randint(10,99)) + '.jpg'
    # print("image name = ", image_name)

    cv2.imwrite(image_name, frame)

def print_out_violation_picture(violations, list_bounding, target_frame):
    # path = 'C:\\Users\\Vinh Luu\\Downloads\\Final_Result\\'
    path = 'D:\\TestDTVC\\Violations\\'

    # path = 'C:\\Users\\ChienDinh\\Desktop\\master\\imgs\\'
    # os.chdir(path)
    frame = copy.copy(target_frame)
    frame = draw_bound(frame, list_bounding)

    for motorbike in violations:
        frame = draw_motorbike(frame, motorbike.left, motorbike.top, motorbike.right, motorbike.bottom)
        text = "pas: " + str(round(motorbike.vio_pas_rate, 2))
        frame = put_Text(frame, motorbike.left, motorbike.top - 60, text)

        text = "cro: " + str(round(motorbike.vio_cro_rate, 2))
        frame = put_Text(frame, motorbike.left, motorbike.top - 45, text)

        text = "enc: " + str(round(motorbike.vio_enc_rate, 2))
        frame = put_Text(frame, motorbike.left, motorbike.top - 30, text)

        text = "hel: " + str(round(motorbike.vio_hel_rate, 2))
        frame = put_Text(frame, motorbike.left, motorbike.top - 5, text)

    # cv2.imshow("anh xe", frame)
    # cv2.waitKey(0)
    image_name = path + str(get_time()) + '.jpg'
    # print("image name = ", image_name)

    cv2.imwrite(image_name, frame)

def print_raw_frame(img):
    # path = 'C:\\Users\\Vinh Luu\\Downloads\\Raw_Frame\\'
    path = 'D:\\TestDTVC\\Raw\\'

    image_name = path + str(get_time()) + "_" + str(random.randint(100, 999)) + '.jpg'
    # print("image name = ", image_name)
    cv2.imwrite(image_name, img)

def print_detect_frame(img):
    # path = 'C:\\Users\\Vinh Luu\\Downloads\\Raw_Frame\\'
    path = 'D:\\TestDTVC\\Detection\\'

    image_name = path + str(get_time()) + "_" + str(random.randint(100, 999)) + '.jpg'
    # print("image name = ", image_name)
    cv2.imwrite(image_name, img)

def print_frame_process(violations):
    # path = 'C:\\Users\\Vinh Luu\\Downloads\\Matched_Frame\\'
    # image_path = 'C:\\Users\\Vinh Luu\\Downloads\\d112020171806_18.jpg'

    path = 'D:\\TestDTVC\\Aggre\\'
    image_path = 'D:\\TestDTVC\\test.jpg'

    target_frame = cv2.imread(image_path)
    frame = copy.copy(target_frame)

    for motorbike in violations:
        frame = draw_motorbike(frame, motorbike.left, motorbike.top, motorbike.right, motorbike.bottom)
        text = "pas: " + str(round(motorbike.vio_pas_rate, 2))
        frame = put_Text(frame, motorbike.left, motorbike.top - 60, text)

        text = "cro: " + str(round(motorbike.vio_cro_rate, 2))
        frame = put_Text(frame, motorbike.left, motorbike.top - 45, text)

        text = "enc: " + str(round(motorbike.vio_enc_rate, 2))
        frame = put_Text(frame, motorbike.left, motorbike.top - 30, text)

        text = "hel: " + str(round(motorbike.vio_hel_rate, 2))
        frame = put_Text(frame, motorbike.left, motorbike.top - 5, text)

    image_name = path + str(get_time()) + "_" + str(random.randint(100, 999)) + '.jpg'
    # print("image name = ", image_name)
    cv2.imwrite(image_name, frame)
