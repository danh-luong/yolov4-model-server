import collections
from service.motorbike_service import detect_passing_red_light
from service.motorbike_service import detect_passing_red_light
from service.motorbike_service import detect_crossing
from service.motorbike_service import detect_encroaching
from service.motorbike_service import detect_not_wearing_helmet

from algorithm.CrossingLine import listCrossing
from algorithm.EncroachingDetection import listEncroaching
from location.LinePoint import LinePoint
from service.lane_service import calculate_distance
from yolov3.configs import *

test = {}
test[0] = 9
test[2] = 3
test[1] = 4
print(test[0])

test = collections.OrderedDict(sorted(test.items()))

# for key in test:
#     print(key, " ->", test[key])

# value = test[0]
# print(value)
#
# for frame in range(1,len(test)):
#     print(test[frame])

horizontalLine = LinePoint(462, 1000, 468, 450)
verticalLine = LinePoint(302, 184, 304, 252)
lane = LinePoint(462, 200, 1000, 230)
lane_distance = calculate_distance(horizontalLine, lane)
print("lane_distance = " , lane_distance)