from yolov3.utils import is_red, detect_image, detect_realtime, detect_video, Load_Yolo_model, detect_video_realtime_mp
import numpy as np
from yolov3.configs import *
import cv2
import concurrent.futures
import datetime
from service.lane_service import calculate_distance
from service.logic import detect_right_violation
from location.LinePoint import LinePoint
from aggregateViolations import process

if __name__ == '__main__':
    yolo = Load_Yolo_model()
    listPositions = {}

    horizontalLine = LinePoint(462, 1000, 468, 450)
    verticalLine = LinePoint(302, 184, 304, 252)
    lane = LinePoint(462, 200, 1000, 230)
    lane_distance = calculate_distance(horizontalLine, lane)
    rate = 1
    sec = 0
    count = 0
    frame = cv2.imread("C:\\Users\\Vinh Luu\\Downloads\\3.jpg")
    sec += 1
    storage_frame = frame
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)
    cv2.imshow("Anh goc", frame)

    arr = [yolo, frame, 416, YOLO_COCO_CLASSES, 0.4, 0.45, '', True]
    with concurrent.futures.ThreadPoolExecutor() as executor:
        id = count
        vilolations = []
        future = executor.submit(lambda p: detect_image(*p), arr)
        tempList = future.result()
        # start detect violation
        traffic_light = tempList['TrafficLight']
        is_red = True
        # is_red = False
        for light in traffic_light:
            is_red = is_red(getattr(light, 'bottom'), getattr(light, 'top'), getattr(
                light, 'left'), getattr(light, 'right'), frame)
            if is_red:
                break
        if is_red and (count <= 3):
            violations = detect_right_violation(
                tempList, horizontalLine, lane_distance, verticalLine, storage_frame)
            if len(violations) > 0:
                listPositions[id] = violations
                count += 1
        else:
            print("-----------------------")
            # print(listPositions)
            final_process = process(listPositions)

            print(final_process)
            # for object in final_process:
            #     print("-----")
            #     print("FINAL RESULT")
            #     print("top: ", object.top, " - left: ", object.left, "- bottom: ", object.bottom, " - right: ", object.right)
            #     # print("img_pas =" , object.img_pas," -> vio_pas_rate = ", object.vio_pas_rate)
            #     # print("img_cro =" , object.img_cro," ->vio_cro_rate = ", object.vio_cro_rate)
            #     # print("img_enc =" , object.img_enc," ->vio_enc_rate = ", object.vio_enc_rate)
            #     # print("img_hel =" , object.img_hel," ->vio_hel_rate = ", object.vio_hel_rate)
            #
            #     print("vio_pas_rate = ", object.vio_pas_rate)
            #     print("vio_cro_rate = ", object.vio_cro_rate)
            #     print("vio_enc_rate = ", object.vio_enc_rate)
            #     print("vio_hel_rate = ", object.vio_hel_rate)
            count = 0
            listPositions = {}
        # end
        # listPositions[str(id)] = tempList
cv2.destroyAllWindows()
